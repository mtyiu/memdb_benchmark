#include <iostream>
#include <iomanip>
#include <time.h>
#include <stdio.h>
#include <stdlib.h>
#include "include/Data.hh"

using namespace std;

Data::Data( int size, int key_length, int value_length ) {
	this->size = size;
	this->key_length = key_length;
	this->value_length = value_length;

	this->isRead = false;

	this->keys = new string[ size ];
	this->values = new string[ size ];
	this->perm = new int[ size ];
}

Data::Data( int &_size, int &_key_length, int &_value_length, char *filename ) {
	FILE *f = fopen( filename, "rb" );
	char *buf, *key_buf, *value_buf;
	int i, tmp;

	this->isRead = true;

	if ( ! f ) {
		cerr << "Cannot read input file." << endl;
		exit( 1 );
	}

	// Read parameters
	if ( fread( &(this->size), sizeof( int ), 1, f ) != 1 ) {
		cerr << "Cannot read \"size\"." << endl;
		goto invalid_file_contents;
	}

	if ( fread( &(this->key_length), sizeof( int ), 1, f ) != 1 ) {
		cerr << "Cannot read \"key_length\"." << endl;
		goto invalid_file_contents;
	}

	if ( fread( &(this->value_length), sizeof( int ), 1, f ) != 1 ) {
		cerr << "Cannot read \"value_length\"." << endl;
		goto invalid_file_contents;
	}

	// Prepare buffer
	this->keys = new string[ this->size ];
	this->values = new string[ this->size ];
	this->perm = new int[ this->size ];

	buf = ( char * ) malloc( sizeof( char ) * ( this->key_length + this->value_length + 2 ) );
	key_buf = buf;
	value_buf = buf + this->key_length + 1;

	// Read arrays
	key_buf[ this->key_length ] = '\0';
	value_buf[ this->value_length ] = '\0';
	for( i = 0; i < this->size; i++ ) {
		if ( fread( key_buf, sizeof( char ) * this->key_length, 1, f ) != 1 ) {
			cerr << "Cannot read \"keys[ " << i << " ]\"." << endl;
			goto invalid_file_contents;
		}
		this->keys[ i ] = std::string( key_buf );
	}

	for( i = 0; i < this->size; i++ ) {
		if ( fread( value_buf, sizeof( char ) * this->value_length, 1, f ) != 1 ) {
			cerr << "Cannot read \"values[ " << i << " ]\"." << endl;
			goto invalid_file_contents;
		}
		this->values[ i ] = std::string( value_buf );
	}

	for( i = 0; i < this->size; i++ ) {
		if ( fread( &tmp, sizeof( int ), 1, f ) != 1 ) {
			cerr << "Cannot read \"perm[ " << i << " ]\"." << endl;
			goto invalid_file_contents;
		}
		this->perm[ i ] = tmp;
	}

	// Return values
	_size = this->size;
	_key_length = this->key_length;
	_value_length = this->value_length;

	::free( buf );
	fclose( f );
	return;

invalid_file_contents:
	fclose( f );
	exit( 1 );
}

void Data::write( char *filename ) {
	FILE *f = fopen( filename, "wb" );
	int i;

	if ( ! f ) {
		cerr << "Cannot write output file." << endl;
		exit( 1 );
	}

	// Read parameters
	if ( fwrite( &(this->size), sizeof( int ), 1, f ) != 1 ) {
		cerr << "Cannot write \"size\"." << endl;
		goto write_fail;
	}

	if ( fwrite( &(this->key_length), sizeof( int ), 1, f ) != 1 ) {
		cerr << "Cannot write \"key_length\"." << endl;
		goto write_fail;
	}

	if ( fwrite( &(this->value_length), sizeof( int ), 1, f ) != 1 ) {
		cerr << "Cannot write \"value_length\"." << endl;
		goto write_fail;
	}

	// Write arrays
	for( i = 0; i < this->size; i++ ) {
		if ( fwrite( this->keys[ i ].c_str(), sizeof( char ) * this->key_length, 1, f ) != 1 ) {
			cerr << "Cannot write \"keys[ " << i << " ]\"." << endl;
			goto write_fail;
		}
	}

	for( i = 0; i < this->size; i++ ) {
		if ( fwrite( this->values[ i ].c_str(), sizeof( char ) * this->value_length, 1, f ) != 1 ) {
			cerr << "Cannot write \"values[ " << i << " ]\"." << endl;
			goto write_fail;
		}
	}

	for( i = 0; i < this->size; i++ ) {
		if ( fwrite( &(this->perm[ i ]), sizeof( int ), 1, f ) != 1 ) {
			cerr << "Cannot write \"perm[ " << i << " ]\"." << endl;
			goto write_fail;
		}
	}

	fclose( f );
	return;

write_fail:
	fclose( f );
	exit( 1 );
}

string Data::random( int len ) {
	static string charset = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
	static int charset_len = charset.length();
	string ret( len, '\0' );
	int i;

	for ( i = 0; i < len; i++ ) {
		ret[ i ] = charset[ rand() % charset_len ];
	}

	return ret;
}

int *Data::random_perm() {
	int i, j;

	if ( this->isRead )
		return this->perm;

	for ( i = 0; i < this->size; i++ ) {
		this->perm[ i ] = -1;
	}
	srand( time( NULL ) + 1 );
	for ( i = 0; i < this->size; i++ ) {
		while( 1 ) {
			j = rand() % this->size;
			if ( this->perm[ j ] == -1 ) {
				this->perm[ j ] = i;
				break;
			}
		}
	}
	return this->perm;
}

void Data::generate() {
	int i;
	srand( time( NULL ) );
	for ( i = 0; i < size; i++ ) {
		this->keys[ i ] = this->random( this->key_length );
		this->values[ i ] = this->random( this->value_length );
	}
}

void Data::print() {
	int i;
	for ( i = 0; i < size; i++ ) {
		cout << '[' << setw( 5 ) << i << "] ";
		cout << this->keys[ i ] << " : " << this->values[ i ] << endl;
	}
	cout << endl;
	cout << "Permutation : " << endl;
	for ( i = 0; i < size; i++ ) {
		cout << setw( 5 ) << this->perm[ i ] << ( i % 10 == 9 ? "\n" : "  " );
	}
	cout << endl;
}

void Data::free() {
	delete[] this->keys;
	delete[] this->values;
	delete[] this->perm;
}
