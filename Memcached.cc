#include <iostream>
#include <fstream>
#include <stdlib.h>
#include "include/Memcached.hh"

using namespace std;

Memcached::Memcached() {
	Memcached( false );
}

Memcached::Memcached( bool isMemC3 ) {
	string line;
	ifstream f( isMemC3 ? "conf/memc3.conf" : "conf/memcached.conf" );
	if ( f.is_open() ) {
		getline( f, line );
		this->config = line;
		f.close();
	} else {
		this->config = "--SERVER=localhost";
	}
	this->isMemC3 = isMemC3;
}

bool Memcached::open() {
	this->memc = memcached( this->config.c_str(), this->config.length() );
	return false;
}

bool Memcached::get( string key, string *value ) {
	size_t value_length;
	uint32_t flags;
	memcached_return_t error;
	char *ret;

	ret = memcached_get( this->memc, key.c_str(), key.length(), &value_length, &flags, &error );
	if ( ret ) {
		value->assign( ret );
		free( ret );
		return true;
	}
	return false;
}

bool Memcached::put( string key, string value ) {
	memcached_return_t rc = memcached_set( this->memc, key.c_str(), key.length(), value.c_str(), value.length(), ( time_t ) 0, ( uint32_t ) 0 );
	// std::cerr << std::string( memcached_last_error_message( this->memc ) ) << std::endl;
	return ( rc == MEMCACHED_SUCCESS );
}

memcached_return_t Memcached::memcached_dump_fn( const memcached_st *memc, const char *key, size_t key_length, void *context ) {
	size_t value_length;
	uint32_t flags;
	memcached_return_t error;
	char *ret;
	ret = memcached_get( ( memcached_st * ) memc, key, key_length, &value_length, &flags, &error );
	if ( ret ) {
		// cout << key << " : " << ret << endl;
		free( ret );
		return MEMCACHED_FAILURE;
	}
	return MEMCACHED_FAILURE;
}

int Memcached::scan( bool print ) {
	void *context = malloc( sizeof( bool ) * 1 );
	bool ret;
	memcached_return_t ( *dump_fns[] )( const memcached_st *, const char *, size_t, void *) = { this->memcached_dump_fn };
	ret = memcached_dump( this->memc, dump_fns, context, 1 );
	free( context );
	return ret;
}

string Memcached::name() {
	return this->isMemC3 ? "MemC3" : "Memcached";
}

Memcached::~Memcached() {
	memcached_free( this->memc );
}
