#include <iostream>
#include <fstream>
#include <stdlib.h>
#include "include/LevelDB.hh"

using namespace std;

LevelDB::LevelDB() {
	string line;
	ifstream f( "conf/leveldb.conf" );
	this->options.create_if_missing = true;
	this->readOptions = leveldb::ReadOptions();
	this->writeOptions = leveldb::WriteOptions();
	if ( f.is_open() ) {
		getline( f, line );
		this->filename = line;
		f.close();
	} else {
		this->filename = "testdb";
	}
}

bool LevelDB::open() {
	leveldb::Status status;
	status = leveldb::DB::Open( this->options, this->filename, &this->db );
	return status.ok();
}

bool LevelDB::get( string key, string *value ) {
	leveldb::Status status = this->db->Get( this->readOptions, key, value );
	bool ret = status.ok();
	if ( ! ret )
		cerr << status.ToString() << endl;
	return ret;
}

bool LevelDB::put( string key, string value ) {
	leveldb::Status status = this->db->Put( this->writeOptions, key, value );
	bool ret = status.ok();
	if ( ! ret )
		cerr << status.ToString() << endl;
	return ret;
}

int LevelDB::scan( bool print ) {
	int count = 0;
	leveldb::Iterator* it = db->NewIterator( this->readOptions );
	for( it->SeekToFirst(); it->Valid(); it->Next(), count++ ) {
		if ( print )
			cout << it->key().ToString() << " : " << it->value().ToString() << endl;
	}
	return count;
}

string LevelDB::name() {
	return "Google LevelDB";
}

LevelDB::~LevelDB() {
	delete this->db;
}
