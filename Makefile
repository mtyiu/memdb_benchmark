LEVELDB_PATH=/home/mtyiu/Development/leveldb
ROCKSDB_PATH=/home/mtyiu/Development/rocksdb
MEMCACHED_PATH=/home/mtyiu/Development/libmemcached-1.0.18
CXX = g++
CXXFLAGS = -Wall -O3 -std=c++11 -Wno-maybe-uninitialized
LDFLAGS += -I$(LEVELDB_PATH)/include -I$(ROCKSDB_PATH)/include -I$(MEMCACHED_PATH) -I. -L$(LEVELDB_PATH) -L$(ROCKSDB_PATH) -lleveldb -lrocksdb -lmemcached -lpthread -lz

SOURCES = \
	Data.cc \
	LevelDB.cc \
	RocksDB.cc \
	Memcached.cc \
	Timer.cc \
	Main.cc

EXE = \
	main

all: $(SOURCES:.cc=.o) main

main: $(SOURCES:.cc=.o)
	$(CXX) $(CXXFLAGS) -o main $(SOURCES:.cc=.o) $(LDFLAGS)

%.o: %.cc
	$(CXX) $(CXXFLAGS) -c $< -o $@ $(LDFLAGS)

clean:
	rm -f *.o $(EXE)
