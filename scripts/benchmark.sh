#!/bin/bash

BENCHMARK_DIR=/home/mtyiu/Development/benchmark

WORKLOAD=2
KEY_LENGTH=32
VALUE_LENGTH=1024
INPUT_DATA=tmp_data

rm -rf /tmp/ramdisk/*db_test 2> /dev/null
rm -rf ${BENCHMARK_DIR}/log
killall memcached memc3
mkdir ${BENCHMARK_DIR}/log
cd ${BENCHMARK_DIR}

# for SIZE in 500000 1000000 1250000; do
for SIZE in 500000; do
	echo "-------------------- Size = ${SIZE} --------------------"
	# Generate data
	${BENCHMARK_DIR}/main 0 3 ${SIZE} ${KEY_LENGTH} ${VALUE_LENGTH} ${INPUT_DATA}
	ls -alh ${INPUT_DATA}

	# Start benchmark
	# Google LevelDB
	DATABASE_DRIVER=1
	${BENCHMARK_DIR}/main ${DATABASE_DRIVER} ${WORKLOAD} ${INPUT_DATA}
	rm -rf /tmp/ramdisk/leveldb_test 2> /dev/null
	echo "#########################################################"
	# Facebook RocksDB
	DATABASE_DRIVER=2
	${BENCHMARK_DIR}/main ${DATABASE_DRIVER} ${WORKLOAD} ${INPUT_DATA}
	rm -rf /tmp/ramdisk/rocksdb_test 2> /dev/null
	echo "#########################################################"
	# memcached
	${BENCHMARK_DIR}/scripts/start_memcached.sh 1> ${BENCHMARK_DIR}/log/memcached.out 2> ${BENCHMARK_DIR}/log/memcached.err &
	sleep 2 # Wait for the service to start
	DATABASE_DRIVER=3
	${BENCHMARK_DIR}/main ${DATABASE_DRIVER} ${WORKLOAD} ${INPUT_DATA}
	killall memcached
	echo "#########################################################"
	# MemC3
	${BENCHMARK_DIR}/scripts/start_memc3.sh 1> ${BENCHMARK_DIR}/log/memc3.out 2> ${BENCHMARK_DIR}/log/memc3.err &
	sleep 2 # Wait for the service to start
	DATABASE_DRIVER=4
	${BENCHMARK_DIR}/main ${DATABASE_DRIVER} ${WORKLOAD} ${INPUT_DATA}
	killall memc3
	echo "#########################################################"
done
