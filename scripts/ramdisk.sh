#!/bin/bash

RAMDISK_PATH=/tmp/ramdisk

sudo umount ${RAMDISK_PATH}
rm -rf ${RAMDISK_PATH}
mkdir ${RAMDISK_PATH}
chmod 777 ${RAMDISK_PATH}

sudo mount -t tmpfs -o size=2500M tmpfs ${RAMDISK_PATH}
