#include <iostream>
#include "include/Timer.hh"

using namespace std;

void Timer::start() {
	this->elapsed = 0;
	clock_gettime( CLOCK_REALTIME, &this->start_time );
}

double Timer::end() {
	struct timespec end_time;
	struct timespec tmp_time;

	clock_gettime( CLOCK_REALTIME, &end_time );

	if ( end_time.tv_nsec - start_time.tv_nsec < 0 ) {
		tmp_time.tv_sec = end_time.tv_sec - start_time.tv_sec - 1;
		tmp_time.tv_nsec = 1000000000 + end_time.tv_nsec - start_time.tv_nsec;
	} else {
		tmp_time.tv_sec = end_time.tv_sec - start_time.tv_sec;
		tmp_time.tv_nsec = end_time.tv_nsec - start_time.tv_nsec;
	}

	this->elapsed = tmp_time.tv_sec + ( tmp_time.tv_nsec / 1.0e9 );

	return this->elapsed;
}

void Timer::print() {
	cout << "Elapsed Time: " << this->elapsed << " s." << endl;
}
