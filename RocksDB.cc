#include <iostream>
#include <fstream>
#include <stdlib.h>
#include "include/RocksDB.hh"

using namespace std;

RocksDB::RocksDB() {
	string line;
	ifstream f( "conf/rocksdb.conf" );
	this->options.create_if_missing = true;
	this->readOptions = rocksdb::ReadOptions();
	this->writeOptions = rocksdb::WriteOptions();
	if ( f.is_open() ) {
		getline( f, line );
		this->filename = line;
		f.close();
	} else {
		this->filename = "testdb";
	}
}

bool RocksDB::open() {
	rocksdb::Status status;
	status = rocksdb::DB::Open( this->options, this->filename, &this->db );
	return status.ok();
}

bool RocksDB::get( string key, string *value ) {
	rocksdb::Status status = this->db->Get( this->readOptions, key, value );
	bool ret = status.ok();
	if ( ! ret )
		cerr << status.ToString() << endl;
	return ret;
}

bool RocksDB::put( string key, string value ) {
	rocksdb::Status status = this->db->Put( this->writeOptions, key, value );
	bool ret = status.ok();
	if ( ! ret )
		cerr << status.ToString() << endl;
	return ret;
}

int RocksDB::scan( bool print ) {
	int count = 0;
	rocksdb::Iterator* it = db->NewIterator( this->readOptions );
	for( it->SeekToFirst(); it->Valid(); it->Next(), count++ ) {
		if ( print )
			cout << it->key().ToString() << " : " << it->value().ToString() << endl;
	}
	return count;
}

string RocksDB::name() {
	return "Facebook RocksDB";
}

RocksDB::~RocksDB() {
	delete this->db;
}
