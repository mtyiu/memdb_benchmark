#ifndef __ROCKSDB_HH__
#define __ROCKSDB_HH__

#include <string>
#include "include/DB.hh"
#include "rocksdb/db.h"

class RocksDB : public DB {
	private:
		rocksdb::DB *db;
		rocksdb::Options options;
		rocksdb::ReadOptions readOptions;
		rocksdb::WriteOptions writeOptions;
		std::string filename;
	public:
		RocksDB();
		bool open();
		bool get( std::string, std::string * );
		bool put( std::string, std::string );
		int scan( bool print );
		std::string name();
		~RocksDB();
};

#endif 
