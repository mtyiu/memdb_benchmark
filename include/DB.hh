#ifndef __DB_HH__
#define __DB_HH__

#include <string>

class DB {
	public:
		virtual bool open() = 0;
		virtual bool get( std::string, std::string * ) = 0;
		virtual bool put( std::string, std::string ) = 0;
		virtual int scan( bool print ) = 0;
		virtual std::string name() = 0;
		virtual ~DB(){};
};

#endif
