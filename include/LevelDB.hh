#ifndef __LEVELDB_HH__
#define __LEVELDB_HH__

#include <string>
#include "include/DB.hh"
#include "leveldb/db.h"

class LevelDB : public DB {
	private:
		leveldb::DB *db;
		leveldb::Options options;
		leveldb::ReadOptions readOptions;
		leveldb::WriteOptions writeOptions;
		std::string filename;
	public:
		LevelDB();
		bool open();
		bool get( std::string, std::string * );
		bool put( std::string, std::string );
		int scan( bool print );
		std::string name();
		~LevelDB();
};

#endif 
