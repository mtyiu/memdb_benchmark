#ifndef __TIMER_HH__
#define __TIMER_HH__

#include <time.h>

class Timer {
	private:
		struct timespec start_time;
		double elapsed;
	public:
		void start();
		double end();
		void print();
};

#endif