#ifndef __MEMCACHED_HH__
#define __MEMCACHED_HH__

#include <string>
#include "include/DB.hh"
#include <libmemcached/memcached.h>

class Memcached : public DB {
	private:
		std::string config;
		memcached_st *memc;
		bool isMemC3;
	public:
		Memcached();
		Memcached( bool isMemC3 );
		bool open();
		bool get( std::string, std::string * );
		bool put( std::string, std::string );
		int scan( bool print );
		static memcached_return_t memcached_dump_fn( const memcached_st *, const char *, size_t, void *);
		std::string name();
		~Memcached();
};

#endif 
