#ifndef __DATA_HH__
#define __DATA_HH__

#include <string>

class Data {
	private:
		int size;
		int key_length;
		int value_length;
		bool isRead;
		
		std::string random( int );
	public:
		std::string *keys;
		std::string *values;
		int *perm;
		
		Data( int, int, int );
		Data( int&, int&, int&, char* );
		void generate();
		int *random_perm();
		void print();
		void write( char* );
		void free();
};

#endif