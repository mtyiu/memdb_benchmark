#include <iostream>
#include <stdlib.h>
#include "include/Data.hh"
#include "include/Timer.hh"
#include "include/DB.hh"
#include "include/LevelDB.hh"
#include "include/RocksDB.hh"
#include "include/Memcached.hh"

#define NUMBER_OF_DB_DRIVERS	4

using namespace std;

int main( int argc, char **argv ) {
	int workload, db_driver, size, key_length, value_length, i, j, index;
	int *random_perm;
	Data *d;
	DB *db = NULL, *dbs[ NUMBER_OF_DB_DRIVERS ];
	string tmp_string;
	Timer timer;

	if ( argc <= 2 ) {
		goto usage;
	}

	db_driver = atoi( argv[ 1 ] );
	workload = atoi( argv[ 2 ] );
	if ( workload != 3 ) {
		switch( db_driver ) {
			case 0:
				dbs[ 0 ] = new LevelDB();
				dbs[ 1 ] = new RocksDB();
				dbs[ 2 ] = new Memcached( false );
				dbs[ 3 ] = new Memcached( true );
				for ( i = 0; i < NUMBER_OF_DB_DRIVERS; i++ ) {
					if ( ! dbs[ i ] ) {
						cerr << "Cannot initialize database #" << i << ": ";
						switch( i ) {
							case 0: cerr << "Google LevelDB"; break;
							case 1: cerr << "Facebook RocksDB"; break;
							case 2: cerr << "Memcached"; break;
							case 3: cerr << "MemC3"; break;
						}
						cerr << "." << endl;
						return 1;
					}
				}
				break;
			case 1:
				db = new LevelDB();
				cout << ">>> Google LevelDB selected. <<<" << endl << endl;
				break;
			case 2:
				db = new RocksDB();
				cout << ">>> Facebook RocksDB selected. <<<" << endl << endl;
				break;
			case 3:
				db = new Memcached( false );
				cout << ">>> Memcached selected. <<<" << endl << endl;
				break;
			case 4:
				db = new Memcached( true );
				cout << ">>> MemC3 selected. <<<" << endl << endl;
				break;
			default:
				goto usage;
		}
	}

	if ( db_driver && ! db ) {
		cerr << "Cannot initialize database." << endl;
		return 1;
	}

	switch( workload ) {
		case 1: goto gen_seq_write_read;
		case 2: goto read_seq_write_read;
		case 3: goto gen_data;
		default: goto usage;
	}

///////////////////////////////////////////////////////////////////////////////

gen_data:
	if ( argc <= 6 ) {
		goto usage;
	}

	size = atoi( argv[ 3 ] );
	key_length = atoi( argv[ 4 ] );
	value_length = atoi( argv[ 5 ] );

	if ( size <= 0 || key_length <= 0 || value_length <= 0 ) {
		goto usage;
	}

	cout << "<<< Generating Data >>>" << endl;
	d = new Data( size, key_length, value_length );
	timer.start();
	d->generate();
	random_perm = d->random_perm();
	timer.end();
	timer.print();
	cout << endl;

	cout << "<<< Writing Data to " << argv[ 6 ] << " >>>" << endl;
	timer.start();
	d->write( argv[ 6 ] );
	timer.end();
	timer.print();
	cout << endl;
	// d->print();

	d->free();
	delete d;

 	return 0;

///////////////////////////////////////////////////////////////////////////////

gen_seq_write_read:
	if ( argc <= 5 ) {
		goto usage;
	}

	size = atoi( argv[ 3 ] );
	key_length = atoi( argv[ 4 ] );
	value_length = atoi( argv[ 5 ] );

	if ( size <= 0 || key_length <= 0 || value_length <= 0 ) {
		goto usage;
	}

	cout << "<<< Generating Data >>>" << endl;
	d = new Data( size, key_length, value_length );
	timer.start();
	d->generate();
	random_perm = d->random_perm();
	timer.end();
	timer.print();
	cout << endl;
	// d->print();
	goto seq_write_read;

read_seq_write_read:
	if ( argc <= 3 ) {
		goto usage;
	}
	cout << "<<< Reading Data >>>" << endl;
	timer.start();
	d = new Data( size, key_length, value_length, argv[ 3 ] );
	random_perm = d->random_perm();
	timer.end();
	timer.print();
	cout << endl;
	// d->print();
	goto seq_write_read;

seq_write_read:
	for ( i = 0; i < NUMBER_OF_DB_DRIVERS; i++ ) {
		if ( db_driver == 0 ) {
			db = dbs[ i ];
			cout << "---------- Database Driver: " << db->name() << " ----------" << endl;
		}

		db->open();
    
		cout << "<<<< Putting " << size << " data >>>>" << endl;
		timer.start();
		for ( j = 0; j < size; j++ ) {
			if ( ! db->put( d->keys[ j ], d->values[ j ] ) ) {
				cerr << "Cannot insert Element #" << j << "." << endl << endl;
				break;
			}
		}
		timer.end();
		timer.print();
		cout << endl;

		///////////////////////////////////////////////////////////////////////
		timer.start();
		if ( db->scan( false ) ) {
			timer.end();
			cout << "<<<< Dumping " << size << " data >>>>" << endl;
			timer.print();
			cout << endl;
		} else {
			timer.end();
		}

		///////////////////////////////////////////////////////////////////////
		
		cout << "<<<< Getting " << size << " data (Sequential) >>>>" << endl;
		timer.start();
		for ( j = 0; j < size; j++ ) {
			if ( ! db->get( d->keys[ j ], &tmp_string ) ) {
				cerr << "Cannot retrieve Element #" << j << " : " << d->keys[ j ] << "." << endl << endl;
				break;
			}
			if ( d->values[ j ].compare( tmp_string ) != 0 ) {
				cerr << "Incorrect value for Element #" << j << " - " << d->keys[ j ] << " : " << d->values[ j ] << " VS " << tmp_string << "." << endl << endl;
				break;
			}
		}
		timer.end();
		timer.print();
		cout << endl;

		///////////////////////////////////////////////////////////////////////
		
		cout << "<<<< Getting " << size << " data (Random) >>>>" << endl;
		timer.start();
		for ( j = 0; j < size; j++ ) {
			index = random_perm[ j ];
			if ( ! db->get( d->keys[ index ], &tmp_string ) ) {
				cerr << "Cannot retrieve Element #" << index << " : " << d->keys[ index ] << "." << endl << endl;
				break;
			}
			if ( d->values[ index ].compare( tmp_string ) != 0 ) {
				cerr << "Incorrect value for Element #" << index << " - " << d->keys[ index ] << " : " << d->values[ index ] << " VS " << tmp_string << "." << endl << endl;
				break;
			}
		}
		timer.end();
		timer.print();
		cout << endl;

		///////////////////////////////////////////////////////////////////////
		
		if ( db_driver > 0 ) {
			break;
		}
	}
	
	d->free();
	delete d;

	if ( db_driver ) {
		delete db;
	} else {
		for ( i = 0; i < NUMBER_OF_DB_DRIVERS; i++ ) {
			delete dbs[ NUMBER_OF_DB_DRIVERS ];
		}
	}

	return 0;

///////////////////////////////////////////////////////////////////////////////

usage:
	cerr << "Usage: " << argv[ 0 ] << " [Database Driver] [Workload No.] [Options]\n" << endl;
	
	cerr << "Database driver:" << endl;
	cerr << "\t0. All" << endl;
	cerr << "\t1. Google LevelDB" << endl;
	cerr << "\t2. Facebook RocksDB" << endl;
	cerr << "\t3. memcached" << endl;
	cerr << "\t4. MemC3" << endl;

	cerr << "Workload No.:" << endl;
	cerr << "\t1. Generate Data + Sequential Write + Sequential Read + Random Read: [Size] [Key Length] [Value Length]" << endl;
	cerr << "\t2. Read Data + Sequential Write + Sequential Read + Random Read: [Data Source]" << endl;
	cerr << "\t3. Generate Data (database driver is ignored): [Size] [Key Length] [Value Length] [Output Path]" << endl;
	return 1;
}
